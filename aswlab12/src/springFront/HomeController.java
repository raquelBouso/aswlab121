package springFront;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import models.ModelException;
import models.Tweet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import util.TimeService;


@Controller
@RequestMapping({"/","/wall"})
@SessionAttributes({"currentDate","wallTweets", "deletable"})
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	@ModelAttribute("tweetForm")
	public TweetForm newTweetForm() {
		TweetForm tweetForm = new TweetForm();
		return tweetForm;
	}

	@ModelAttribute("loginForm")
	public LoginForm newLoginForm() {
		LoginForm loginForm = new LoginForm();
		return loginForm;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String browse(HttpSession session, Model model) {

		logger.info("Entering into the main page");

		String userName = (String) session.getAttribute("loggedUserNAME");
		Vector<HashMap<String, Object>> wallTweets = new Vector<HashMap<String, Object>>();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int deletable = 0;
		try {
			Collection<Tweet> tweets = Tweet.findAll();

			for (Tweet tweet : tweets) {
				HashMap<String,Object> tweetHash = new HashMap<String, Object>();
				tweetHash.put("tweetID", tweet.getTweet_id());
				tweetHash.put("authorID", tweet.getUser_id());
				tweetHash.put("content", tweet.getText());
				tweetHash.put("likes", tweet.getLikes());
				java.util.Date temps = mySQLTimeStamp.parse(tweet.getTime(), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				String authorName = tweet.getUser_name();
				tweetHash.put("authorName", authorName);
				if (userName !=null && userName.equals(authorName)) deletable++;
				wallTweets.addElement(tweetHash);
			}

			model.addAttribute("currentDate", TimeService.getCurrentDate());
			model.addAttribute("wallTweets",wallTweets);
			model.addAttribute("deletable",deletable);
			model.addAttribute("loggedUserNAME", userName);
			model.addAttribute("likedTable",session.getAttribute("likedTable"));

			logger.info("Rendering the main page");

			return "browser";
		}
		catch (ModelException ex) {
			model.addAttribute("theList",ex.getMessageList());
			return "error";
		}
	}

}
